# Статистика в R. Часть 1. Применение дисперсионного анализа (ANOVA - Analysis of Variance)

library(ggplot2)

# 0. reading data
shops_data     <- read.csv("https://stepic.org/media/attachments/lesson/11505/shops.csv")
therapy_data   <- read.csv("https://stepic.org/media/attachments/lesson/11505/therapy_data.csv")
pillulkin_data <- read.csv("https://stepic.org/media/attachments/lesson/11505/Pillulkin.csv")

therapy_data$subject   <- as.factor(therapy_data$subject)
pillulkin_data$patient <- as.factor(pillulkin_data$patient)


# 1. Однофакторный Дисперсионный Анализ (One-way ANOVA)
# Он однофакторный, потому что мы используем только одну 
# Независимую Переменную (Фактор), для исследования её влияния
# на Зависимую Переменную.
#
# Часто в исследованиях возникает необходимость сравнивать несколько групп между собой.
# Та переменная, которая, будет разделять наши наблюдения на группы (номенативная переменная 
# с несколькими градациями) называется Независимой Переменной. А та переменная (количественная),
# в которой мы сравнимаем группы, называется Зависимой.
boxplot(price ~ origin, data = shops_data)
ggplot(shops_data, aes(x = origin, y = price)) + geom_boxplot()

# aov выполняет Дисперсионный Анализ,
# где переменые price - Зависимая, origin - Независимая
fit <- aov(price ~ origin, data = shops_data)
# чтобы посмотреть различия групп надо вызвать summary
summary(fit)


# 2. Two-way ANOVA
fit <- aov(price ~ origin + store, data = shops_data)
summary(fit)

model.tables(fit, "mean")


# 3. Попарное сравнение
# Оценим визуально
ggplot(shops_data, aes(x = food, y = price)) + geom_boxplot()
# Видно, что разные группы товаров существенно отличаются по ценам
# Посмотрим это с помощью Дисперсионного Анализа
fit <- aov(price ~ food, data = shops_data)
summary(fit) # p-value < 0.05, что позволяет отбросить H0 о равенстве средних на все группы товаров
# Однако мы не можем сказать какие группы товаров различаются между собой
# Для этого необходимо попарное сравнение групп между собой
TukeyHSD(fit)


# 4. Дисперсионный анализ с повторными измерениями
# Попробуем сначало построить Однофакторную Дисперсионную модель
fit <- aov(well_being ~ therapy, data = therapy_data)
summary(fit) # p-value > 0.05, т.е. Разные терапии (в том числе плацебо) не различаются
# как это делается с учётом повторных измерений (одна и та же тарапия проводилась с каждым испытуемым)
fit <- aov(well_being ~ therapy + Error(subject/therapy), data = therapy_data)
summary(fit)

fit <- aov(well_being ~ therapy*price, data = therapy_data)
summary(fit)
# Из отчёта видно, что самым значимым фактором, влияющим на оценку самочувствия оказалась цена на терапию
# Оценим это на графике
ggplot(therapy_data, aes(x = price, y = well_being)) + geom_boxplot()
# Построим ту же модесь, но с учётом повторных измерений
fit <- aov(well_being ~ therapy*price + Error(subject/(therapy*price)), data = therapy_data)
summary(fit)
ggplot(therapy_data, aes(x = price, y = well_being)) + geom_boxplot() + facet_grid(~subject)

fit <- aov(well_being ~ therapy*price*sex, data = therapy_data)
summary(fit)

fit <- aov(well_being ~ therapy*price*sex + Error(subject/(therapy*price)), data = therapy_data)
summary(fit)


#--------------------------------------
# home task 1
# Воспользуемся встроенными данными npk, иллюстрирующими влияние применения различных удобрений на
# урожайность гороха (yield). Нашей задачей будет выяснить, существенно ли одновременное применение
# азота (фактор N) и фосфата (фактор P). Примените дисперсионный анализ, где будет проверяться влияние
# фактора применения азота (N), влияние фактора применения фосфата (P) и их взаимодействие.
# В ответе укажите p-value для взаимодействия факторов N и P.
res <- aov(yield ~ N*P, data = npk)
summary(res) # 0.4305

# home task 2
# Теперь проведите трехфакторный дисперсионный анализ, где зависимая переменная - это урожайность (yield),
# а три фактора - типы удобрений (N, P, K). После проведения данного анализа вы получите три значения 
# p - уровня значимости (о значимости каждого из факторов).
res <- aov(yield ~ N + K + P, data = npk)
summary(res)

# home task 3
# Проведите однофакторный дисперсионный анализ на встроенных данных iris. Зависимая переменная - ширина 
# чашелистика (Sepal.Width), независимая переменная - вид (Species). Затем проведите попарные сравнения 
# видов. Какие виды статистически значимо различаются по ширине чашелистика (p < 0.05)?
res <- aov(Sepal.Width ~ Species, data = iris)
TukeyHSD(res) # все
ggplot(iris, aes(x = Species, y = Sepal.Width)) + geom_boxplot()

# home task 4
# В этой задаче вам дан набор данных, в котором представлена информация о температуре нескольких пациентов,
# которые лечатся разными таблетками и у разных врачей.
# Проведите однофакторный дисперсионный анализ с повторными измерениями: влияние типа таблетки (pill) на
# температуру (temperature) с учётом испытуемого (patient).
# Каково p-value для влияния типа таблеток на температуру?
res <- aov(temperature ~ pill + Error(patient/pill), data = pillulkin_data)
summary(res) # 0.826

# home task 5
# Теперь вашей задачей будет провести двухфакторный дисперсионный анализ с повторными измерениями:
# влияние факторов doctor, влияние фактора pill и их взаимодействие на temperature. Учтите обе
# внутригрупповые переменные: и тот факт, что один и тот же больной принимает разные таблетки,
# и тот факт, что  один и тот же больной лечится у разных врачей! Каково F-значение для взаимодействия
# факторов доктора (doctor) и типа таблеток (pill)?
res <- aov(temperature ~ pill * doctor + Error(patient/(pill*doctor)), data = pillulkin_data)
summary(res) # 0.146

# home task 6
# Вспомните графики из лекций и дополните шаблон графика в поле для ответа так (не добавляя еще
# один geom), чтобы объединить линиями точки, принадлежащие разным уровням фактора supp.
# Добавил в aes group = supp и всё!
ggplot(ToothGrowth, aes(x = as.factor(dose), y = len, col = supp, group=supp)) +
  stat_summary(fun.data = mean_cl_boot, geom = 'errorbar', width = 0.1, position = position_dodge(0.2)) +
  stat_summary(fun.data = mean_cl_boot, geom = 'point', size = 3, position = position_dodge(0.2)) +
  stat_summary(fun.data = mean_cl_boot, geom = 'line', position = position_dodge(0.2))
