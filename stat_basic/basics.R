# 1. Операции с векторами
my_vector1 <- 1:67
my_vector <- c(-32, 45, 67, 12.78, 129, 0, -65)

my_vector1[1]
my_vector[c(1,2,3)]
my_vector[1:3]

the_best_vector <- c(1:5000, 7000:10000)

my_number <- 1:20
my_number_2 <- my_number[c(2,5,7,9,12,16,20)]

my_vector[my_vector > 0]
my_vector[my_vector < 0]


my_vector1[my_vector1 > 20 & my_vector1 < 30]

v1 <- c(165, 178, 180, 181, 167, 178, 187, 167, 187)

v1[v1 > mean(v1)]

my_sum <- sum(my_vector[my_vector > 10])

v1[v1 > mean(v1) - sd(v1) & v1 < mean(v1) + sd(v1)]


# 2. Работа с наборами данных
x_var <- read.csv("https://stepic.org/media/attachments/lesson/11481/evals.csv")
mean(x_var[x_var$gender == "female",1])
mean(x_var[x_var$gender != "female",1])

head(subset(x_var, gender == "female"))
head(subset(x_var, score > 4))

my_data <- mtcars
my_data$even_gear <- 1 - my_data$gear %% 2


if ( mean(my_vector) > 20 ) {
  result <- "My mean is great"
} else {
  result <- "My mean is not so great"
}


prev <- AirPassengers[1]
good_months <- c()
for ( i in 2:length(AirPassengers) ) {
  if ( AirPassengers[i] > prev ) {
    good_months <- c(good_months, AirPassengers[i])
  }
  prev <- AirPassengers[i]
}

rng <- 10
cnt <- length(AirPassengers)
cs <- cumsum(AirPassengers)
moving_average <- (cs[rng:cnt] - c(0, cs[1:(cnt-rng)])) / rng


df <- mtcars

str(df)

df$vs <- factor(df$vs, labels = c("V", "S"))
df$am <- factor(df$am, labels = c("Auto", "Manual"))

median(df$mpg)
mean(df$disp)
sd(df$hp)
range(df$cyl)

mean(df$mpg[df$cyl == 6])
mean(df$mpg[df$cyl == 6 & df$vs == "V"])

sd(df$hp[df$cyl != 3 & df$am == "Auto"])


result <- mean(mtcars$qsec[mtcars$cyl != 3 & mtcars$mpg > 20])

?aggregate
mean_hg_vs <- aggregate(x = df$hp, by = list(df$vs), FUN = mean)
colnames(mean_hg_vs) <- c("VS", "Mean HP")

aggregate(hp ~ vs, df, mean)
aggregate(hp ~ vs + am, df, mean)
aggregate(x = df$hp, by = list(df$vs, df$am), FUN = mean)
aggregate(x = df[,-c(8,9)], by = list(df$am), FUN = median)
aggregate(x = df[,c(1,3)], by = list(df$am, df$vs), FUN = sd)
aggregate(cbind(mpg, disp) ~ am + vs, df, sd)

cbind(df$mpg, df$disp)


descriptions_stat <- agregate(cbind(hp, disp) ~ am, mtcars, sd)

library(psych)
?describe

describe(x = df[,-c(8,9)])

describeBy(x = df[,-c(8,9)], group = df$vs, mat = T, digits = 1)

describeBy(df$qsec, group = list(df$vs, df$am), mat = T, digits = 1, fast = T)

sum(is.na(df$mpg))

aq <- subset(airquality, Month %in% c(7, 8, 9))
result <- aggregate(Ozone ~ Month, aq, length)

describeBy(x = airquality$Wind, group = airquality$Month, mat = T)
describeBy(x = iris[,-c(5)], group = iris[,-c(5)], mat = T)

round(apply(iris[1:4], 2, sd), 2)
aggregate(x = iris[,c(1,2,3,4)], by = list(iris$Species), FUN = median)


my_vector[is.na(my_vector)] <- mean(my_vector, na.rm = T)



df <- mtcars
df$vs <- factor(df$vs, labels = c("V", "S"))
df$am <- factor(df$am, labels = c("Auto", "Manual"))

hist(df$mpg, breaks = 20)
boxplot(mpg ~ am, df, ylab = "MPG")
plot(df$mpg, df$hp)

# ggplot2.org
# install.packages("ggplot2")
library(ggplot2)

ggplot(df, aes(x = mpg)) + geom_histogram(fill = "white", col = "black", binwidth = 2)
ggplot(df, aes(x = mpg, fill = am)) + geom_dotplot()
ggplot(df, aes(x = mpg)) + geom_density(fill = "blue")
ggplot(df, aes(x = mpg, fill = am)) + geom_density(alpha = 0.4)
ggplot(df, aes(x = am, y = hp, col = vs)) + geom_boxplot()
ggplot(df, aes(x = mpg, y = hp, col = vs)) + geom_point(size = 4, alpha = 0.6)
ggplot(df, aes(x = mpg, y = hp, col = vs, size = qsec)) + geom_point(alpha = 0.6)


my_plot <- ggplot(df, aes(x = mpg, y = hp, col = vs))
my_plot + geom_point(size = 4, alpha = 0.6)
my_plot + geom_boxplot()


#--------------------------------------
# home task 1
ggplot(airquality, aes(x = Month, y = Ozone, group = Month)) + geom_boxplot()

# home task 2
plot1 <- ggplot(mtcars, aes(x = mpg, y = disp, col = hp)) + geom_point()

# home task 3
ggplot(iris, aes(Sepal.Length)) + geom_histogram(aes(fill = Species))
ggplot(iris, aes(Sepal.Length, fill = Species)) + geom_histogram()

# home task 4
ggplot(iris, aes(x = Sepal.Length, y = Sepal.Width, col = Species, size = Petal.Length)) + geom_point()
